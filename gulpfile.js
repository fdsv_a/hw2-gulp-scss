import autoprefixer from "gulp-autoprefixer";
import clean from "gulp-clean";
import cleanCss from "gulp-clean-css";
import imagemin from "gulp-imagemin";
import minifyjs from "gulp-js-minify";
import gulp from "gulp";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import browserSync from "browser-sync";

const BS = browserSync.create();
const sass = gulpSass(dartSass);

const buildStyles = () =>
  gulp
    .src("./src/scss/**/*.scss")
    .pipe(sass())
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(cleanCss())
    .pipe(gulp.dest("./dist/css"));

const buildJS = () =>
  gulp.src("./src/js/*.js").pipe(minifyjs()).pipe(gulp.dest("./dist/js"));

const moveImages = () =>
  gulp.src("./src/image/**/*").pipe(imagemin()).pipe(gulp.dest("./dist/img"));

const cleaner = () => gulp.src("./dist/**/*").pipe(clean());

export const build = gulp.series(cleaner, buildStyles, buildJS, moveImages);

export const dev = gulp.series(build, () => {
  BS.init({
    server: {
      baseDir: "./",
    },
  });

  gulp.watch(
    "./src/**/*",
    gulp.series(build, (done) => {
      BS.reload();
      done();
    })
  );
});
